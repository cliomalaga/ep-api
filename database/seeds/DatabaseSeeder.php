<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$this->call(UsersSeeder::class);
		$this->call(RolesSeeder::class);
		$this->call(ProfilesSeeder::class);
		$this->call(ConfigurationsSeeder::class);
		$this->call(LogactionsSeeder::class);
		$this->call(LogsectionsSeeder::class);
		$this->call(CategoriesSeeder::class);
		$this->call(SubcategoriesSeeder::class);
	}

}
