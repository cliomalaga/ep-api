<?php

use Illuminate\Database\Seeder;

class SubcategoriesSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$data = [
				['id_category' => '1', 'name' => 'Inteligencia'],
				['id_category' => '1', 'name' => 'Personalidad'],
				['id_category' => '1', 'name' => 'Aptitudes'],
				['id_category' => '1', 'name' => 'Clínica adultos'],
				['id_category' => '1', 'name' => 'Clínica infantil'],
				['id_category' => '1', 'name' => 'Educativos'],
				['id_category' => '1', 'name' => 'RRHH'],
				['id_category' => '1', 'name' => 'Generales'],
				['id_category' => '1', 'name' => 'Clínicos'],
				['id_category' => '1', 'name' => 'Infantiles - juveniles'],
				['id_category' => '1', 'name' => 'Forense'],
				['id_category' => '2', 'name' => 'Generales'],
				['id_category' => '2', 'name' => 'Clínicas'],
				['id_category' => '2', 'name' => 'Infantiles - juveniles'],
				['id_category' => '2', 'name' => 'Educativas'],
				['id_category' => '2', 'name' => 'RRHH'],
				['id_category' => '2', 'name' => 'Forense'],
				['id_category' => '3', 'name' => 'Proceso'],
				['id_category' => '3', 'name' => 'Instrumentos'],
				['id_category' => '3', 'name' => 'Validación'],
				['id_category' => '3', 'name' => 'Estadística'],
				['id_category' => '3', 'name' => 'Tecnologías'],
				['id_category' => '4', 'name' => 'Prácticas'],
				['id_category' => '4', 'name' => 'Apuntes'],
				['id_category' => '4', 'name' => 'Evaluación'],
				['id_category' => '4', 'name' => 'Actividades'],
				['id_category' => '4', 'name' => 'Didácticas'],
				['id_category' => '4', 'name' => 'Experiencias'],
				['id_category' => '4', 'name' => 'Innovaciones'],
				['id_category' => '4', 'name' => 'Tecnologías'],
				['id_category' => '5', 'name' => 'Clínica infantil'],
				['id_category' => '5', 'name' => 'Clínica adultos'],
				['id_category' => '5', 'name' => 'Educación'],
				['id_category' => '5', 'name' => 'RRHH'],
				['id_category' => '6', 'name' => 'Proceso'],
				['id_category' => '6', 'name' => 'Instrumentos'],
				['id_category' => '6', 'name' => 'Validación'],
				['id_category' => '6', 'name' => 'Estadística'],
				['id_category' => '6', 'name' => 'Tecnologías'],
		];

		DB::table('subcategories')->insert($data);
	}

}
