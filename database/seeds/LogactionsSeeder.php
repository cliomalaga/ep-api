<?php

use Illuminate\Database\Seeder;

class LogactionsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$data = [
				['id' => 1, 'name' => 'creado'],
				['id' => 2, 'name' => 'actualizado'],
				['id' => 3, 'name' => 'eliminado'],
				['id' => 4, 'name' => 'activado'],
				['id' => 5, 'name' => 'desactivado'],
				['id' => 6, 'name' => 'enviado'],
		];

		DB::table('logactions')->insert($data);
	}

}
