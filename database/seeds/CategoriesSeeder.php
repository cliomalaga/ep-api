<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$data = [
				['id' => 1, 'name' => 'Pruebas'],
				['id' => 2, 'name' => 'Preguntas'],
				['id' => 3, 'name' => 'Documentos'],
				['id' => 4, 'name' => 'Enseñanzas'],
				['id' => 5, 'name' => 'Casos'],
				['id' => 6, 'name' => 'Investigaciones'],
		];

		DB::table('categories')->insert($data);
	}

}
