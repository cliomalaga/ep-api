<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$roles = [
				['level' => '1', 'name' => 'SuperUsuario'],
				['level' => '2', 'name' => 'Administrador'],
				['level' => '3', 'name' => 'Colaborador'],
				['level' => '4', 'name' => 'Usuario'],
		];
		DB::table('roles')->insert($roles);
	}

}
