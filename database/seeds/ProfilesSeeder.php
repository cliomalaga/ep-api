<?php

use Illuminate\Database\Seeder;

class ProfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('profiles')->insert([
			'id_user' => 1,
			'id_rol' => 1,
			'active' => 1,
			'name' => 'Gabriel',
			'lastname' => 'Valero',
			'avatar' => 'assets/images/profile-default.jpg',
		]);
    }
}
