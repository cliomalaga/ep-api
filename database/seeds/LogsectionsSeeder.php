<?php

use Illuminate\Database\Seeder;

class LogsectionsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$data = [
				['id' => 1, 'name' => 'la configuración'],
				['id' => 2, 'name' => 'el perfil'],
				['id' => 3, 'name' => 'un usuario'],
				['id' => 4, 'name' => 'un email'],
				['id' => 5, 'name' => 'un evento'],
				['id' => 6, 'name' => 'una prueba'],
				['id' => 7, 'name' => 'una pregunta'],
				['id' => 8, 'name' => 'un documento'],
				['id' => 9, 'name' => 'una enseñanza'],
				['id' => 10, 'name' => 'un caso'],
				['id' => 11, 'name' => 'una investigación'],
		];

		DB::table('logsections')->insert($data);
	}

}
