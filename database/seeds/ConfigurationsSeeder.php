<?php

use Illuminate\Database\Seeder;

class ConfigurationsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('configurations')->insert([
			'id_user' => 1,
			'animations' => 1,
			'theme_basic' => 0,
			'color_primary' => '#303641',
			'delay_notification' => 5,
			'delay_locked' => 30,
			'view' => 'grid',
			'view_calendar' => 'month',
		]);
	}

}
