<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('configurations', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('id_user')->unsigned();
			$table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
			$table->tinyInteger('animations');
			$table->tinyInteger('theme_basic');
			$table->string('color_primary', 255)->nullable();
			$table->integer('delay_notification');
			$table->integer('delay_locked');
			$table->string('view', 255)->nullable();
			$table->string('view_calendar', 255)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('configurations');
	}

}
