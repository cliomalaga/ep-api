<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('profiles', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('id_user')->unsigned();
			$table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
			$table->tinyInteger('active')->default(1);
			$table->string('name', 255)->nullable();
			$table->string('lastname', 255)->nullable();
			$table->text('about')->nullable();
			$table->string('avatar', 500)->nullable();
			$table->string('phone', 255)->nullable();
			$table->string('Facebook', 500)->nullable();
			$table->string('Twitter', 500)->nullable();
			$table->string('Linkedin', 500)->nullable();
			$table->string('Google', 500)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('profiles');
	}

}
