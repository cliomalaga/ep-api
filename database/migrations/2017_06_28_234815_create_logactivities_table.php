<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogactivitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('logactivities', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('id_user')->unsigned();
			$table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
			$table->integer('id_user_original')->unsigned();
			$table->foreign('id_user_original')->references('id')->on('users')->onDelete('cascade');
			$table->integer('id_logaction')->unsigned();
			$table->foreign('id_logaction')->references('id')->on('logactions')->onDelete('cascade');
			$table->integer('id_logsection')->unsigned();
			$table->foreign('id_logsection')->references('id')->on('logsections')->onDelete('cascade');
			$table->integer('id_item');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('logactivities');
	}

}
