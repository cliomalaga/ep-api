<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestigationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('investigations', function (Blueprint $table) {
			$table->increments('id');
			$table->tinyInteger('active')->default(0);
			$table->integer('id_user')->unsigned();
			$table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
			$table->integer('id_subcategory')->unsigned()->nullable();
			$table->foreign('id_subcategory')->references('id')->on('subcategories')->onDelete('cascade');
			$table->string('title', 255)->nullable();
			$table->string('subtitle', 255)->nullable();
			$table->text('text')->nullable();
			$table->string('image', 255)->nullable();
			$table->string('attached', 255)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('investigations');
	}

}
