<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('subcategories', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('id_category')->unsigned();
			$table->foreign('id_category')->references('id')->on('categories')->onDelete('cascade');
			$table->string('name', 255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('subcategories');
	}

}
