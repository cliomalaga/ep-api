<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('profiles', function (Blueprint $table) {
			$table->integer('id_rol')->unsigned()->after('id_user');
			$table->foreign('id_rol')->references('id')->on('roles')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('profiles', function (Blueprint $table) {
			$table->dropForeign(['id_rol']);
			$table->dropColumn('id_rol');
		});
	}

}
