<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable {

	use Notifiable;
	use SoftDeletes;

	protected $fillable = [
		'email',
		'password',
	];
	protected $hidden = [
		'password', 'remember_token',
	];
	protected $dates = ['deleted_at'];

	public function profile() {
		return $this->hasOne('App\Profile', 'id_user');
	}

}
