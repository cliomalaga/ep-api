<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model {

	protected $table = 'profiles';
	public $timestamps = true;
	protected $fillable = [
		'id_user',
		'id_rol',
		'name',
		'lastname',
		'about',
		'avatar',
		'phone',
		'Facebook',
		'Twitter',
		'Linkedin',
		'Google',
		'active'
	];

	public function user() {
		return $this->belongsTo('App\User', 'id_user')->withTrashed();
	}

	public function rol() {
		return $this->belongsTo('App\Roles', 'id_rol');
	}

}
