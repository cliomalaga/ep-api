<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table = 'categories';
	public $timestamps = false;
	protected $fillable = [
		'name',
	];

	public function category() {
		return $this->belongsTo('App\Category', 'id');
	}

	public function subcategories() {
		return $this->hasMany('App\Subcategory', 'id_category');
	}

}
