<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model {

	protected $table = 'tests';
	public $timestamps = true;
	protected $fillable = [
		'active',
		'id_user',
		'id_subcategory',
		'title',
		'subtitle',
		'text',
		'image',
		'attached',
	];

	public function user() {
		return $this->belongsTo('App\User', 'id_user')->withTrashed();
	}

	public function subcategory() {
		return $this->belongsTo('App\Subcategory', 'id_subcategory');
	}

}
