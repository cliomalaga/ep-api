<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model {

	protected $table = 'logactivities';
	public $timestamps = true;
	protected $fillable = [
		'id_user',
		'id_user_original',
		'id_logaction',
		'id_logsection',
		'id_item'
	];

	public function user() {
		return $this->belongsTo('App\User', 'id_user')->withTrashed();
	}

	public function user_original() {
		return $this->belongsTo('App\User', 'id_user_original')->withTrashed();
	}

	public function action() {
		return $this->belongsTo('App\LogAction', 'id_logaction');
	}

	public function section() {
		return $this->belongsTo('App\LogSection', 'id_logsection');
	}

}
