<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model {

	protected $table = 'configurations';
	public $timestamps = true;
	protected $fillable = [
		'id_user',
		'animations',
		'theme_basic',
		'color_primary',
		'delay_notification',
		'delay_locked',
		'view',
		'view_calendar',
	];

	public function user() {
		return $this->belongsTo('App\User', 'id_user')->withTrashed();
		;
	}

}
