<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model {

	protected $table = 'emails';
	public $timestamps = true;
	protected $fillable = [
		'id_user',
		'emails',
		'subject',
		'messagge',
	];

	public function user() {
		return $this->belongsTo('App\User', 'id_user')->withTrashed();
		;
	}

}
