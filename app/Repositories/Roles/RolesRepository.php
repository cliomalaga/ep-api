<?php

namespace App\Repositories\Roles;

use App\Roles;

class RolesRepository {

	public function all() {
		return Roles::all();
	}

	public function allOrderBy($field, $orden = 'DESC') {
		return Roles::orderBy($field, $orden)->get();
	}

	public function lastRol() {
		return Roles::orderBy('id', 'DESC')->first();
	}

}
