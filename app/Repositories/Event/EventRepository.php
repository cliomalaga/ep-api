<?php

namespace App\Repositories\Event;

use App\Event;

class EventRepository {

	public function get($id) {
		return Event::find($id);
	}

	public function all() {
		return Event::all()->orderBy('');
	}

	public function allOrderBy($field, $orden = 'DESC') {
		return Event::orderBy($field, $orden)->get();
	}

	public function insert(array $data) {
		return Event::create($data);
	}

	public function update($id, array $data) {
		return Event::find($id)->update($data);
	}

	public function delete($id) {
		return Event::destroy($id);
	}

}
