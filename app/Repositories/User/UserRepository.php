<?php

namespace App\Repositories\User;

use App\User;

class UserRepository {

	public function get($id) {
		return User::find($id);
	}

	public function getByEmail($email) {
		return User::where('email', $email)->first();
	}

	public function getById($id) {
		return User::where('id', $id)->first();
	}

	public function allOrderBy($field, $orden = 'DESC') {
		return User::orderBy($field, $orden)->get();
	}

	public function insert(array $data) {
		return User::create($data);
	}

	public function update($id, array $data) {
		return User::find($id)->update($data);
	}

	public function delete($id) {
		return User::destroy($id);
	}

}
