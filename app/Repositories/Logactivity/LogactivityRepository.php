<?php

namespace App\Repositories\Logactivity;

use App\LogActivity;

class LogactivityRepository {

	public function get($id) {
		return LogActivity::find($id);
	}

	public function getByUser($id) {
		return LogActivity::where('id_user', $id)->first();
	}

	public function all() {
		return LogActivity::all();
	}

	public function allOrderBy($field, $orden = 'DESC') {
		return LogActivity::orderBy($field, $orden)
				->limit(200)
				->get();
	}

	public static function insert(array $data) {
		return LogActivity::create($data);
	}

	public function update($id, array $data) {
		return LogActivity::find($id)->update($data);
	}

	public function delete($id) {
		return LogActivity::destroy($id);
	}

}
