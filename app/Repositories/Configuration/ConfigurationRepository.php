<?php

namespace App\Repositories\Configuration;

use App\Configuration;

class ConfigurationRepository {

	public function get($id) {
		return Configuration::find($id);
	}

	public function getByUser($id_user) {
		return Configuration::where('id_user', $id_user)
			->first();
	}
	public function getOwn() {
		return Configuration::where('id_user', \Auth::user()->id)
			->first();
	}

	public function insert(array $data) {
		return Configuration::create($data);
	}

	public function update($id, array $data) {
		return Configuration::find($id)->update($data);
	}
	
	public function deleteByUser($id_user) {
		$element = $this->getByUser($id_user);
		return Configuration::destroy($element->id);
	}
}
