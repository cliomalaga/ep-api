<?php

namespace App\Repositories\Category;

use App\Category;

class CategoryRepository {

	public function allOrderBy($field, $orden = 'DESC') {
		$elements = Category::orderBy($field, $orden)->get();
		return $elements;
	}

	public function getNameById($id) {
		return Category::where('id', $id)->first(['name'])->name;
	}

}
