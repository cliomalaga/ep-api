<?php

namespace App\Repositories\Email;

use App\Email;

class EmailRepository {

	public function get($id) {
		return Email::find($id);
	}

	public function all() {
		return Email::all();
	}

	public function allOrderBy($field, $orden = 'DESC') {
		return Email::orderBy($field, $orden)->get();
	}

	public function insert(array $data) {
		return Email::create($data);
	}

}
