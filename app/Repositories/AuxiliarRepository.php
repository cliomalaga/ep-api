<?php

namespace App\Repositories;

use App\Category;
use App\Subcategory;
use Carbon\Carbon;

class AuxiliarRepository {

	public static function search($model, $string) {
		$elements = $model::where('title', 'LIKE', '%' . $string . '%')
			->orWhere('subtitle', 'LIKE', '%' . $string . '%')
			->orWhere('text', 'LIKE', '%' . $string . '%')
			->where('active', '=', 1)
			->get();
		foreach ($elements as $element) {
			$element = self::parseItemsFront($element);
			$element->title = str_ireplace($string, '<mark>' . $string . '</mark>', $element->title);
			$element->subtitle = str_ireplace($string, '<mark>' . $string . '</mark>', $element->subtitle);
			$element->text = str_ireplace($string, '<mark>' . $string . '</mark>', $element->text);
		}
		return $elements;
	}

	public static function get($model, $id) {
		$element = $model::find($id);
		$element = self::parseItemsFront($element);
		return $element;
	}

	public static function getActive($model, $id) {
		$element = $model::where('active', '=', 1)->where('id', '=', $id)->first();
		$element = self::parseItemsFront($element);
		return $element;
	}

	public static function getByUser($model, $id) {
		$elements = $model::where('active', '=', 1)->where('id_user', '=', $id)->get();
		foreach ($elements as $element) {
			$element = self::parseItemsFront($element);
		}
		return $elements;
	}

	public static function getPrevious($model, $id) {
		$idPrevious = $model::where('active', '=', 1)->where('id', '<', $id)->max('id');
		$element = self::get($model, $idPrevious);
		return $element;
	}

	public static function getNext($model, $id) {
		$idNext = $model::where('active', '=', 1)->where('id', '>', $id)->min('id');
		$element = self::get($model, $idNext);
		return $element;
	}

	public static function getItemsFront($model, $number) {
		$elements = $model::where('active', '=', 1)->orderBy('created_at', 'DESC')->take($number)->get();
		foreach ($elements as $element) {
			$element = self::parseItemsFront($element);
		}
		return $elements;
	}

	public static function getRandom($model, $number) {
		$elements = $model::where('active', '=', 1)->get();
		$n = (count($elements) < $number) ? count($elements) : $number;
		$elements = $elements->random($n);
		if (count($elements) == 1) {
			$elements = array($elements);
		}
		foreach ($elements as $element) {
			$element = self::parseItemsFront($element);
		}
		return $elements;
	}

	public static function getTimeline($model, $number) {
		$elements = $model::where('active', '=', 1)->orderBy('created_at', 'DESC')->take($number)->get();
		foreach ($elements as $element) {
			$element = self::parseItemsFront($element);
		}
		return $elements;
	}

	public static function getOlded($model, $date) {
		$date = new Carbon($date);
		$elements = $model::where('active', '=', 1)->orderBy('created_at', 'DESC')->whereDate('created_at', '<=', $date)->get();
		foreach ($elements as $element) {
			$element = self::parseItemsFront($element);
		}
		return $elements;
	}

	public static function getBySubcategory($model, $ids, $number = null) {
		$elements = $model::where('active', '=', 1)->orderBy('created_at', 'DESC')->whereIn('id_subcategory', $ids)->get();
		// If number is empty, search all
		if (!empty($number)) {
			$n = (count($elements) < $number) ? count($elements) : $number;
			$elements = $elements->random($n);
		}
		// If exist register
		if (count($elements) > 0) {
			// If only exit one register, we force array type
			if (empty($elements[0])) {
				$elements = [$elements];
			}
			// Parse items to front
			foreach ($elements as $element) {
				$element = self::parseItemsFront($element);
			}
		}
		return $elements;
	}

	public static function all($model) {
		$elements = $model::where('active', '=', 1)->orderBy('created_at', 'DESC')->get();
		foreach ($elements as $element) {
			$element = self::parseItemsFront($element);
		}
		return $elements;
	}

	public static function allOrderBy($model, $field, $orden = 'DESC') {
		$elements = $model::where('active', '=', 1)->orderBy($field, $orden)->get();
		foreach ($elements as $element) {
			$element = self::parseItemsFront($element);
		}
		return $elements;
	}

	public static function insert($model, $data) {
		$element = $model::create($data);
		$element = self::parseItemsFront($element);
		return $element;
	}

	public static function update($model, $id, $data) {
		$model::find($id)->update($data);
		$element = $model::find($id);
		$element = self::parseItemsFront($element);
		return $element;
	}

	public static function delete($model, $id) {
		$element = $model::find($id);
		$element = self::parseItemsFront($element);
		$model::destroy($id);
		return $element;
	}

	/*
	 *
	 *
	 */
	/* PARSE FIELD BEFORE SEND TO FRONT */

	public static function parseItemsFront($element) {
		if (!empty($element) && !$element->parsed) {
			//Flag to check parse item
			$element->parsed = true;

			// If exist description
			if (!empty($element->description)) {
				$element->description_pretty = str_replace("<div>", " ", $element->description);
				$element->description_pretty = strip_tags($element->description_pretty);
			}

			// If exist text
			if (!empty($element->text)) {
				$element->text_pretty = str_replace("<div>", " ", $element->text);
				$element->text_pretty = strip_tags($element->text_pretty);
			}

			// Profile
			$user = $element->user;
			if (!empty($user->profile)) {
				$element->created_by = self::parseProfileFront($user->profile);
			}

			// Delete user information
			unset($element->user);


			// If exist id_subcategory
			if (!empty($element->id_subcategory)) {
				// Get category and subcategory
				$subcategory = Subcategory::find($element->id_subcategory);
				$category = Category::find($subcategory->id_category);

				// Add field category and subcategory url
				$subcategory->url = str_slug($category->name) . '/' . str_slug($subcategory->name);
				$category->url = str_slug($category->name);

				// Add fields to item
				$element->category = $category;
				$element->subcategory = $subcategory;
				$element->path = str_slug($element->title) . '-' . $element->id;
				$element->url = $subcategory->url . '/' . $element->path;
			} else {
				$element->url = '';
			}
		}

		return $element;
	}

	public static function parseProfileFront($profile) {
		if (!empty($profile) && !$profile->parsed) {
			//Flag to check parse item
			$profile->parsed = true;
			// Get category and subcategory
			$profile->email = $profile->user->email;
			$profile->path = str_slug($profile->name . ' ' . $profile->lastname) . '-' . $profile->id_user;
			$profile->url = '/profesores/' . $profile->path;
			unset($profile->user);
		}

		return $profile;
	}

}
