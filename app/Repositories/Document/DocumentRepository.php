<?php

namespace App\Repositories\Document;

use App\Document;
use App\Repositories\AuxiliarRepository;

class DocumentRepository {

	public function __construct(Document $model) {
		$this->model = $model;
	}

	public function search($string) {
		return AuxiliarRepository::search($this->model, $string);
	}

	public function get($id) {
		return AuxiliarRepository::get($this->model, $id);
	}

	public function getActive($id) {
		return AuxiliarRepository::getActive($this->model, $id);
	}

	public function getByUser($id) {
		return AuxiliarRepository::getByUser($this->model, $id);
	}

	public function getPrevious($id) {
		return AuxiliarRepository::getPrevious($this->model, $id);
	}

	public function getNext($id) {
		return AuxiliarRepository::getNext($this->model, $id);
	}

	public function getItemsFront($number) {
		return AuxiliarRepository::getItemsFront($this->model, $number);
	}

	public function getRandom($number) {
		return AuxiliarRepository::getRandom($this->model, $number);
	}

	public function getTimeline($number) {
		return AuxiliarRepository::getTimeline($this->model, $number);
	}

	public function getOlded($date) {
		return AuxiliarRepository::getOlded($this->model, $date);
	}

	public function getBySubcategory($ids, $number = null) {
		return AuxiliarRepository::getBySubcategory($this->model, $ids, $number);
	}

	public function all() {
		return AuxiliarRepository::all($this->model);
	}

	public function allOrderBy($field, $orden = 'DESC') {
		return AuxiliarRepository::allOrderBy($this->model, $field, $orden);
	}

	public function insert(array $data) {
		return AuxiliarRepository::insert($this->model, $data);
	}

	public function update($id, array $data) {
		return AuxiliarRepository::update($this->model, $id, $data);
	}

	public function delete($id) {
		return AuxiliarRepository::delete($this->model, $id);
	}

}
