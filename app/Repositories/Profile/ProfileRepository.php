<?php

namespace App\Repositories\Profile;

use App\Profile;
use App\Repositories\AuxiliarRepository;

class ProfileRepository {

	public function __construct(Profile $model) {
		$this->model = $model;
	}

	public function get($id) {
		return Profile::find($id);
	}

	public function getActive($id) {
		return AuxiliarRepository::getActive($this->model, $id);
	}

	public function getByUser($id) {
		return Profile::where('id_user', $id)->first();
	}

	public function all() {
		return Profile::all();
	}

	public function allActive() {
		return Profile::where('active', '=', 1)->get();
	}

	public function allOrderBy($field, $orden = 'DESC') {
		return Profile::orderBy($field, $orden)->get();
	}

	public function insert(array $data) {
		return Profile::create($data);
	}

	public function update($id, array $data) {
		return Profile::find($id)->update($data);
	}

	public function delete($id) {
		return Profile::destroy($id);
	}

	public function parseFront($profile) {
		return AuxiliarRepository::parseProfileFront($profile);
	}

}
