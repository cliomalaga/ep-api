<?php

namespace App\Repositories\Subcategory;

use App\Subcategory;

class SubcategoryRepository {

	public function get($id) {
		return Subcategory::find($id);
	}

	public function getIdsByName($subcategory) {
		$elements = Subcategory::where('name', 'like', '%' . str_replace("-", " - ", $subcategory) . '%')->get(['id']);
		// Replace with space if not found register
		if(count($elements)==0){
			$elements = Subcategory::where('name', 'like', '%' .  str_replace("-", " ", $subcategory) . '%')->get(['id']);
		}
		return $elements;
	}

	public function getNameById($id) {
		return Subcategory::where('id', $id)->first(['name'])->name;
	}

	public function all() {
		return Subcategory::all();
	}

	public function allOrderBy($field, $orden = 'DESC') {
		return Subcategory::orderBy($field, $orden)->get();
	}

	public function insert(array $data) {
		return Subcategory::create($data);
	}

	public function update($id, array $data) {
		return Subcategory::find($id)->update($data);
	}

	public function delete($id) {
		return Subcategory::destroy($id);
	}

}
