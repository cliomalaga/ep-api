<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model {

	protected $table = 'subcategories';
	public $timestamps = false;
	protected $fillable = [
		'id_category',
		'name',
	];

	public function category() {
		return $this->belongsTo('App\Category', 'id_category');
	}

}
