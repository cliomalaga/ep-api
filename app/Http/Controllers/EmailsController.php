<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Helpers\Helper;
use App\Repositories\Email\EmailRepository;
use App\Repositories\Logactivity\LogactivityRepository;
use Lang;
use Mail;

class EmailsController extends Controller {

	protected $email;
	protected $logactivity;
	protected $section;
	protected $sectionTranslate;

	/*
	 *
	 */
	/* CONSTRUCT */

	public function __construct(EmailRepository $email, LogactivityRepository $logactivity) {
		$this->email = $email;
		$this->logactivity = $logactivity;
		$this->section = 'emails';
		$this->sectionTranslate = 'mensajes';
	}

	/*
	 *
	 *
	 */
	/* LIST ALL */

	public function index() {
		if (isAdmin()) {
			$elements = $this->email->allOrderBy('id', 'DESC');
			foreach ($elements as $element) {
				$element = $this->parseFieldsEmails($element);
			}
			return responseOK($elements, NULL);
		} else {
			return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.noautorizado-r'));
		}
	}

	/*
	 *
	 *
	 */
	/* LIST INDIVIDUAL */

	public function show($id) {
		if (isAdmin()) {
			//If register exist
			if ($element = $this->email->get($id)) {
				$element = $this->parseFieldsEmails($element);
				return responseOK($element, NULL);
			} else {
				return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
			}
		} else {
			return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.noautorizado-r'));
		}
	}

	/*
	 *
	 *
	 */
	/* SEND MAIL */

	public function sendEmail(Request $request) {
		if (isAdmin()) {
			$post = $request->all();
			if (!empty($post['messagge']) && !empty($post['subject']) && count($post['emails']) > 0) {
				$data = $post;
				$data['toEmail'] = \Auth::user()->email;
				// Send message
				Mail::send('email/email', $data, function ($message) use ($data) {
					$message->to($data['emails']);
					$message->sender($data['toEmail']);
					$message->replyTo($data['toEmail']);
					$message->subject($data['subject']);
				});
				// If message was not sent
				if (Mail::failures()) {
					return responseKO('500', Lang::trans('messages.' . $this->sectionTranslate . '.nocorrecto'));
				}
				// fill data is message is sent to save on DB
				$data = array(
					'id_user' => \Auth::user()->id,
					'emails' => json_encode(parseParam('emails', $post)),
					'subject' => parseParam('subject', $post),
					'messagge' => parseParam('messagge', $post),
				);
				//Create register
				if ($element = createBD($data, $this->email)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'send', $this->section, $element);
					// Response OK
					$element = $this->parseFieldsEmails($element);
					return responseOK($element, Lang::trans('messages.' . $this->sectionTranslate . '.correcto'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noanadido'));
				}
			} else {
				return responseKO('500', Lang::trans('messages.' . $this->sectionTranslate . '.faltancampos'));
			}
		} else {
			return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.noautorizado-s'));
		}
	}

	/*
	 *
	 *
	 */
	/* PARSE FIELD BEFORE SEND TO FRONT */

	public function parseFieldsEmails($element) {
		$element->emails = json_decode($element->emails);

		// If exist messagge
		if (!empty($element->messagge)) {
			$element->messagge_pretty = str_replace("<div>", " ", $element->messagge);
			$element->messagge_pretty = strip_tags($element->messagge_pretty);
		}

		// Profile
		$user = $element->user;
		if (!empty($user->profile)) {
			$element->created_by = $user->profile;
		}

		return $element;
	}

}
