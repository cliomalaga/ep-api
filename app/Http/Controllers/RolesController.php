<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\Roles\RolesRepository;
use Lang;

class RolesController extends Controller {

	protected $roles;

	/*
	 *
	 */
	/* CONSTRUCT */

	public function __construct(RolesRepository $roles) {
		$this->roles = $roles;
		$this->section = 'roles';
		$this->sectionTranslate = 'roles';
	}

	/*
	 *
	 *
	 */
	/* LIST ALL */

	public function index() {
		$elements = $this->roles->allOrderBy('level', 'ASC');
		return responseOK($elements, NULL);
	}

}
