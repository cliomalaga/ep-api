<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Helpers\Helper;
use App\Repositories\Logactivity\LogactivityRepository;
use Lang;

class LogactivitiesController extends Controller {

	protected $logactivity;
	protected $section;
	protected $sectionTranslate;

	/*
	 *
	 */
	/* CONSTRUCT */

	public function __construct(LogactivityRepository $logactivity) {
		$this->logactivity = $logactivity;
	}

	/*
	 *
	 *
	 */
	/* LIST ALL */

	public function index() {
		$elements = $this->logactivity->allOrderBy('id', 'DESC');
		foreach ($elements as $element) {
			$element = $this->parseFields($element);
		}
		return responseOK($elements, NULL);
	}

	/*
	 *
	 *
	 */
	/* PARSE FIELD BEFORE SEND TO FRONT */

	public function parseFields($element) {
		$element->logaction = $element->action->name;
		$element->logsection = $element->section->name;
		$element->user_name = $element->user->profile->name . ' ' . $element->user->profile->lastname;
		$element->user_avatar = $element->user->profile->avatar;
		$element->user_original_name = $element->user_original->profile->name . ' ' . $element->user_original->profile->lastname;
		$element->link = idToSection($element->id_logsection);

		unset($element->action);
		unset($element->section);
		unset($element->user);
		unset($element->user_original);

		return $element;
	}

}
