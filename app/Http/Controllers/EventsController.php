<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Helpers\Helper;
use App\Repositories\Event\EventRepository;
use App\Repositories\Logactivity\LogactivityRepository;
use Lang;

class EventsController extends Controller {

	protected $event;
	protected $logactivity;
	protected $section;
	protected $sectionTranslate;

	/*
	 *
	 */
	/* CONSTRUCT */

	public function __construct(EventRepository $event, LogactivityRepository $logactivity) {
		$this->event = $event;
		$this->logactivity = $logactivity;
		$this->section = 'events';
		$this->sectionTranslate = 'evento';
	}

	/*
	 *
	 *
	 */
	/* LIST ALL */

	public function index() {
		$elements = $this->event->allOrderBy('id', 'DESC');
		foreach ($elements as $element) {
			$element = $this->parseFields($element);
		}
		return responseOK($elements, NULL);
	}

	/*
	 *
	 *
	 */
	/* LIST INDIVIDUAL */

	public function show($id) {
		//If register exist
		if ($element = $this->event->get($id)) {
			$element = $this->parseFields($element);
			return responseOK($element, NULL);
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 *
	 */
	/*
	  /* CREATE */

	public function store(Request $request) {
		//Get params
		$post = $request->all();
		//If not exist image, return fields set NULL
		$image = uploadBase64('image', 'image', $this->section, $post);
		//Fill data
		$data = array(
			'id_user' => \Auth::user()->id,
			'description' => parseParam('description', $post),
			'image' => parseParam('image', $image),
			'datetime_start' => correctDateTimeToDB(parseParam('datetime_start', $post), parseParam('time_start', $post)),
			'datetime_end' => correctDateTimeToDB(parseParam('datetime_end', $post), parseParam('time_end', $post)),
			'active' => parseParam('active', $post),
		);
		//Create register
		if ($element = createBD($data, $this->event)) {
			// Log Activity
			registerLogActivity($this->logactivity, 'create', $this->section, $element);
			// Response OK
			$element = $this->parseFields($element);
			return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.anadido'));
		} else {
			return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noanadido'));
		}
	}

	/*
	 *
	 *
	 */
	/* UPDATE */

	public function update(Request $request, $id) {
		// If register exist
		if ($event = $this->event->get($id)) {
			// If user is item owner
			if (isOwn($event)) {
				//Get params
				$post = $request->all();
				//If not exist image, return fields set NULL
				$image = uploadBase64('image', 'image', $this->section, $post);
				//Fill data
				$data = array(
					'description' => parseParam('description', $post),
					'image' => parseParam('image', $image),
					'datetime_start' => correctDateTimeToDB(parseParam('datetime_start', $post), parseParam('time_start', $post)),
					'datetime_end' => correctDateTimeToDB(parseParam('datetime_end', $post), parseParam('time_end', $post)),
					'active' => parseParam('active', $post),
				);
				//Update fields
				if ($element = updateBD($id, $data, $this->event)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'update', $this->section, $element);
					// Response OK
					$element = $this->parseFields($element);
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.actualizado'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noactualizado'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-u'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 *
	 */
	/* ACTIVATE */

	public function activate($id) {
		// If register exist
		if ($event = $this->event->get($id)) {
			// If user is item owner
			if (isOwn($event)) {
				if ($element = updateBD($id, array('active' => 1), $this->event)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'activate', $this->section, $element);
					// Response OK
					$element = $this->parseFields($element);
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.activado'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noactivado'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-a'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 *
	 */
	/* DESACTIVATE */

	public function desactivate($id) {
		// If register exist
		if ($event = $this->event->get($id)) {
			// If user is item owner
			if (isOwn($event)) {
				if ($element = updateBD($id, array('active' => 0), $this->event)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'desactivate', $this->section, $element);
					// Response OK
					$element = $this->parseFields($element);
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.desactivado'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.nodesactivado'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-d'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 */
	/* DELETE */

	public function destroy($id) {
		// If register exist
		if ($element = $this->event->get($id)) {
			// If user is item owner
			if (isOwn($element)) {
				// Delete data
				if ($this->event->delete($id)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'delete', $this->section, $element);
					// Response OK
					return responseOK(NULL, Lang::get('messages.' . $this->sectionTranslate . '.eliminado'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noeliminado'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-e'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 *
	 */
	/* PARSE FIELD BEFORE SEND TO FRONT */

	public function parseFields($element) {
		$element->datetime_start = correctDateTimeToFront($element->datetime_start);
		$element->time_start = parseTimeToFront($element->datetime_start);
		$element->datetime_end = correctDateTimeToFront($element->datetime_end);
		$element->time_end = parseTimeToFront($element->datetime_end);

		// If exist description
		if (!empty($element->description)) {
			$element->description_pretty = str_replace("<div>", " ", $element->description);
			$element->description_pretty = strip_tags($element->description_pretty);
		}

		// Profile
		$user = $element->user;
		if (!empty($user->profile)) {
			$element->created_by = $user->profile;
		}

		return $element;
	}

}
