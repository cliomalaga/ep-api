<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\Test\TestRepository;
use App\Repositories\Document\DocumentRepository;
use App\Repositories\Casee\CaseeRepository;
use App\Repositories\Teaching\TeachingRepository;
use App\Repositories\Investigation\InvestigationRepository;
use App\Repositories\Faq\FaqRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Subcategory\SubcategoryRepository;
use App\Repositories\Profile\ProfileRepository;
use Lang;
use Mail;

class FrontendController extends Controller {

	protected $test;
	protected $document;
	protected $casee;
	protected $teaching;
	protected $investigation;
	protected $faq;
	protected $category;
	protected $subcategory;
	protected $profile;
	protected $maxCategories;

	public function __construct(TestRepository $test, DocumentRepository $document, CaseeRepository $casee, TeachingRepository $teaching, InvestigationRepository $investigation, FaqRepository $faq, CategoryRepository $category, SubcategoryRepository $subcategory, ProfileRepository $profile) {
		$this->test = $test;
		$this->document = $document;
		$this->casee = $casee;
		$this->teaching = $teaching;
		$this->investigation = $investigation;
		$this->faq = $faq;
		$this->category = $category;
		$this->subcategory = $subcategory;
		$this->profile = $profile;
		$this->maxCategories = 3;
	}

	/*
	 *
	 * EMAILS
	 *
	 */

	public function sendEmail(Request $request) {
		$post = $request->all();
		if (!empty($post['name']) && !empty($post['email']) && !empty($post['messagge'])) {
			$data = $post;
			$data['toEmail'] = 'info@evaluacionpsicologica.es';
			// Send message
			Mail::send('email/email-contact', $data, function ($message) use ($data) {
				$message->to($data['toEmail']);
				$message->sender('evaluacionpsicologicasmtp@gmail.com');
				$message->subject('Formulario de Contacto');
			});
			if (Mail::failures()) {
				return responseKO('500', 'El mensaje no pudo ser enviado, por favor inténtelo de nuevo más tarde.');
			} else {
				return responseOK('OK', 'Mensaje enviado correctamente.');
			}
		} else {
			return responseKO('500', 'El mensaje no pudo ser enviado, faltan campos obligatorios.');
		}
	}

	/*
	 *
	 * SEARCH
	 *
	 */

	public function search(Request $request) {
		$items = [];
		$post = $request->all();
		$string = trim($post['search']);
		if (strlen($string) > 0) {
			$test = $this->test->search($string);
			$faq = $this->faq->search($string);
			$casee = $this->casee->search($string);
			$teaching = $this->teaching->search($string);
			$investigation = $this->investigation->search($string);
			$items = array_merge($test->toArray(), $casee->toArray(), $teaching->toArray(), $investigation->toArray());
			// Sort by date
			usort($items, $this->_sortArrayBy('created_at', 'DESC'));

			return $items;
		}
		return $items;
	}

	/*
	 *
	 * MENU
	 *
	 */

	public function getMenu() {
		$data = [];
		$categories = $this->category->allOrderBy('id', 'ASC');
		foreach ($categories as $category) {
			$data['submenu' . $category->id] = $category->subcategories;
		}
		return responseOK($data, NULL);
	}

	/*
	 *
	 * HOME
	 *
	 */

	public function getItemsHome() {
		$data = [];
		$number = 5;
		$data['test'] = $this->test->getItemsFront($number);
		$data['casee'] = $this->casee->getItemsFront($number);
		$data['teaching'] = $this->teaching->getItemsFront($number);
		$data['investigation'] = $this->investigation->getItemsFront($number);
		$data['faq'] = $this->faq->getItemsFront($number);

		return responseOK($data, NULL);
	}

	public function getFaqsHome() {
		$number = 12;
		$data = $this->faq->getItemsFront($number);
		return responseOK($data, NULL);
	}

	public function getRandomHome() {
		$data = $this->_getItemsRandom(3);
		return responseOK($data, NULL);
	}

	/*
	 *
	 * SIDEBAR
	 *
	 */

	public function getArchive(Request $request) {
		$post = $request->all();
		$date = 'Last Month';
		$test = $this->test->getOlded($date);
		$faq = $this->faq->getOlded($date);
		$casee = $this->casee->getOlded($date);
		$teaching = $this->teaching->getOlded($date);
		$investigation = $this->investigation->getOlded($date);

		$arrayMerge = array_merge($test->toArray(), $casee->toArray(), $teaching->toArray(), $investigation->toArray());
		usort($arrayMerge, $this->_sortArrayBy('created_at', 'DESC'));
		$data = array_slice($arrayMerge, 0, $post['number']);

		return responseOK($data, NULL);
	}

	public function getLast() {
		$data = $this->_getTimeline(1);
		return responseOK($data, NULL);
	}

	public function getTimeline(Request $request) {
		$post = $request->all();
		$data = $this->_getTimeline($post['number']);

		return responseOK($data, NULL);
	}

	public function getGallery() {
		$data = [];
		$items = $this->_getItemsRandom(6);
//		$data = $this->_getItemsRandom(8);
		foreach ($items as $item) {
			$data[] = $item;
		}
		return responseOK($data, NULL);
	}

	public function getSameCategory(Request $request) {
		$post = $request->all();
		$repository = $this->_getRepositoryById($post['id_category']);
		$data = $repository->getRandom(3);

		return responseOK($data, NULL);
	}

	public function getSameSubcategory(Request $request) {
		$post = $request->all();
		$ids = [$post['id_subcategory']];
		$repository = $this->_getRepositoryById($post['id_category']);
		$data = $repository->getBySubcategory($ids);

		return responseOK($data, NULL);
	}

	/*
	 *
	 * CATEGORY
	 *
	 */

	public function getCategory($category) {
		$data = $this->_getCategory($category, $this->maxCategories);
		return responseOK($data, NULL);
	}

	public function getCategoryArchive($category) {
		$data = $this->_getCategory($category);
		return responseOK($data, NULL);
	}

	/*
	 *
	 * SUBCATEGORY
	 *
	 */

	public function getSubcategory($category, $subcategory) {
		$data = $this->_getSubcategory($category, $subcategory, $this->maxCategories);
		return responseOK($data, NULL);
	}

	public function getSubcategoryArchive($category, $subcategory) {
		$data = $this->_getSubcategory($category, $subcategory);
		return responseOK($data, NULL);
	}

	/*
	 *
	 * ITEM
	 *
	 */

	public function showFrontend($category, $subcategory, $path) {
		$data = [];
		// Get id according URL
		$id = pathToId($path);
		// Get respository according Url
		$repository = $this->_getRepositoryByName($category);
		// Get item
		$data['item'] = $repository->getActive($id);
		$data['previous'] = $repository->getPrevious($id);
		$data['next'] = $repository->getNext($id);
		$data['relateds'] = $repository->getRandom(3);

		return responseOK($data, NULL);
	}

	/*
	 *
	 * TEACHER
	 *
	 */

	public function getTeachers() {
		$data = [];
		$data['profiles'] = $this->profile->allActive();
		foreach ($data['profiles'] as $profile) {
			$profile = $this->profile->parseFront($profile);
		}
		return responseOK($data, NULL);
	}

	public function getTeacher($path) {
		$data = [];
		// Get id according URL
		$id = pathToId($path);
		// Get profile according Url
		$profile = $this->profile->getByUser($id);
		$data['profile'] = $this->profile->parseFront($profile);
		$data['items'] = $this->_getItemsByUser($id);

		return responseOK($data, NULL);
	}

	/*
	 *
	 * AUXILIAR FUNCTIONS
	 *
	 */

	public function _getRepositoryById($id) {
		switch ($id) {
			case '1': $repository = $this->test;
				break;
			case '2': $repository = $this->faq;
				break;
			case '3': $repository = $this->document;
				break;
			case '4': $repository = $this->teaching;
				break;
			case '5': $repository = $this->casee;
				break;
			case '6': $repository = $this->investigation;
				break;
		}
		return $repository;
	}

	public function _getIdByName($category) {
		switch ($category) {
			case 'pruebas':$repository = 1;
				break;
			case 'preguntas':$repository = 2;
				break;
			case 'documentos':$repository = 3;
				break;
			case 'ensenanzas':$repository = 4;
				break;
			case 'casos':$repository = 5;
				break;
			case 'investigaciones':$repository = 6;
				break;
		}
		return $repository;
	}

	public function _getRepositoryByName($category) {
		switch ($category) {
			case 'pruebas':$repository = $this->test;
				break;
			case 'preguntas':$repository = $this->faq;
				break;
			case 'documentos':$repository = $this->document;
				break;
			case 'ensenanzas':$repository = $this->teaching;
				break;
			case 'casos':$repository = $this->casee;
				break;
			case 'investigaciones':$repository = $this->investigation;
				break;
		}
		return $repository;
	}

	public function _getTimeline($number) {
		$data = [];
		$test = $this->test->getTimeline($number);
		$faq = $this->faq->getTimeline($number);
		$casee = $this->casee->getTimeline($number);
		$teaching = $this->teaching->getTimeline($number);
		$investigation = $this->investigation->getTimeline($number);

		$data = array_merge($test->toArray(), $casee->toArray(), $teaching->toArray(), $investigation->toArray());
		usort($data, $this->_sortArrayBy('created_at', 'DESC'));
		$data = array_slice($data, 0, $number);

		return $data;
	}

	public function _getItemsByUser($id) {
		$test = $this->test->getByUser($id);
		$faq = $this->faq->getByUser($id);
		$casee = $this->casee->getByUser($id);
		$teaching = $this->teaching->getByUser($id);
		$investigation = $this->investigation->getByUser($id);
		$data = array_merge($test->toArray(), $casee->toArray(), $teaching->toArray(), $investigation->toArray());
		// Sort by date
		usort($data, $this->_sortArrayBy('created_at', 'DESC'));
		// Or shuffle array
		// shuffle($data);

		return $data;
	}

	public function _getItemsRandom($number) {
		$test = $this->test->getRandom($number);
		$faq = $this->faq->getRandom($number);
		$casee = $this->casee->getRandom($number);
		$teaching = $this->teaching->getRandom($number);
		$investigation = $this->investigation->getRandom($number);
		$arrayMerge = array_merge($test->toArray(), $casee->toArray(), $teaching->toArray(), $investigation->toArray());
		shuffle($arrayMerge);
		$data = array_slice($arrayMerge, 0, $number);

		return $data;
	}

	public function _getCategory($category, $number = null) {
		$repository = $this->_getRepositoryByName($category);
		$id_category = $this->_getIdByName($category);
		$data['category_name'] = $this->category->getNameById($id_category);
		$data['category_url'] = str_slug($data['category_name']);
		$data['items'] = $repository->all();
		if (!empty($number)) {
			$items = [];
			foreach ($data['items'] as $item) {
				$items[] = $item;
			}
			$data['items'] = array_slice($items, 0, $this->maxCategories);
		}

		return $data;
	}

	public function _getSubcategory($category, $subcategory, $number = null) {
		$data = [];
		$ids = $this->subcategory->getIdsByName($subcategory);
		if (!empty($ids)) {
			$id_subcategory = $ids[0]->id;
			$id_category = $this->_getIdByName($category);
			$repository = $this->_getRepositoryByName($category);
			$data['category_name'] = $this->category->getNameById($id_category);
			$data['category_url'] = str_slug($data['category_name']);
			$data['subcategory_name'] = $this->subcategory->getNameById($id_subcategory);
			$data['subcategory_url'] = str_slug($data['subcategory_name']);
			$data['items'] = $repository->getBySubcategory($ids);
			if (!empty($number)) {
				$items = [];
				foreach ($data['items'] as $item) {
					$items[] = $item;
				}
				$data['items'] = array_slice($items, 0, $this->maxCategories);
			}
		}
		return $data;
	}

	private function _sortArrayBy($key, $dir = 'ASC') {
		return function ($a, $b) use ($key, $dir) {
			$t1 = strtotime(is_array($a) ? $a[$key] : $a->$key);
			$t2 = strtotime(is_array($b) ? $b[$key] : $b->$key);
			if ($t1 == $t2)
				return 0;
			return (strtoupper($dir) == 'ASC' ? ($t1 < $t2) : ($t1 > $t2)) ? -1 : 1;
		};
	}

}
