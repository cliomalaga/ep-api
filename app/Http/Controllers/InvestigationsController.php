<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Helpers\Helper;
use App\Repositories\Investigation\InvestigationRepository;
use App\Repositories\Logactivity\LogactivityRepository;
use Lang;

class InvestigationsController extends Controller {

	protected $investigation;
	protected $logactivity;
	protected $section;
	protected $sectionTranslate;

	/*
	 *
	 */
	/* CONSTRUCT */

	public function __construct(InvestigationRepository $investigation, LogactivityRepository $logactivity) {
		$this->investigation = $investigation;
		$this->logactivity = $logactivity;
		$this->section = 'investigations';
		$this->sectionTranslate = 'investigación';
	}

	/*
	 *
	 *
	 */
	/* LIST ALL */

	public function index() {
		$elements = $this->investigation->allOrderBy('id', 'DESC');
		return responseOK($elements, NULL);
	}

	/*
	 *
	 *
	 */
	/* LIST INDIVIDUAL */

	public function show($id) {
		//If register exist
		if ($element = $this->investigation->get($id)) {
			return responseOK($element, NULL);
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrada'));
		}
	}

	/*
	 *
	 *
	 */
	/*
	  /* CREATE */

	public function store(Request $request) {
		//Get params
		$post = $request->all();
		//If not exist image, return fields set NULL
		$image = uploadBase64('image', 'image', $this->section, $post);
		$attached = uploadBase64('attached', 'attached', $this->section, $post);
		//Fill data
		$data = array(
			'active' => parseParam('active', $post),
			'id_user' => \Auth::user()->id,
			'id_subcategory' => parseParam('id_subcategory', $post),
			'title' => parseParam('title', $post),
			'subtitle' => parseParam('subtitle', $post),
			'text' => parseParam('text', $post),
			'image' => parseParam('image', $image),
			'attached' => parseParam('attached', $attached),
		);
		//Create register
		if ($element = createBD($data, $this->investigation)) {
			// Log Activity
			registerLogActivity($this->logactivity, 'create', $this->section, $element);
			// Response OK
			return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.anadida'));
		} else {
			return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noanadida'));
		}
	}

	/*
	 *
	 *
	 */
	/* UPDATE */

	public function update(Request $request, $id) {
		// If register exist
		if ($investigation = $this->investigation->get($id)) {
			// If user is item owner
			if (isOwn($investigation)) {
				//Get params
				$post = $request->all();
				//If not exist image, return fields set NULL
				$image = uploadBase64('image', 'image', $this->section, $post);
				$attached = uploadBase64('attached', 'attached', $this->section, $post);
				//Fill data
				$data = array(
					'active' => parseParam('active', $post),
					'id_subcategory' => parseParam('id_subcategory', $post),
					'title' => parseParam('title', $post),
					'subtitle' => parseParam('subtitle', $post),
					'text' => parseParam('text', $post),
					'image' => parseParam('image', $image),
					'attached' => parseParam('attached', $attached),
				);
				if (empty($data['id_subcategory'])) {
					$data['id_subcategory'] = null;
				}
				//Update fields
				if ($element = updateBD($id, $data, $this->investigation)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'update', $this->section, $element);
					// Response OK
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.actualizada'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noactualizada'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-u'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrada'));
		}
	}

	/*
	 *
	 *
	 */
	/* ACTIVATE */

	public function activate($id) {
		// If register exist
		if ($investigation = $this->investigation->get($id)) {
			// If user is item owner
			if (isOwn($investigation)) {
				if ($element = updateBD($id, array('active' => 1), $this->investigation)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'activate', $this->section, $element);
					// Response OK
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.activada'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noactivada'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-a'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrada'));
		}
	}

	/*
	 *
	 *
	 */
	/* DESACTIVATE */

	public function desactivate($id) {
		// If register exist
		if ($investigation = $this->investigation->get($id)) {
			// If user is item owner
			if (isOwn($investigation)) {
				if ($element = updateBD($id, array('active' => 0), $this->investigation)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'desactivate', $this->section, $element);
					// Response OK
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.desactivada'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.nodesactivada'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-d'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrada'));
		}
	}

	/*
	 *
	 */
	/* DELETE */

	public function destroy($id) {
		// If register exist
		if ($element = $this->investigation->get($id)) {
			// If user is item owner
			if (isOwn($element)) {
				// Delete data
				if ($this->investigation->delete($id)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'delete', $this->section, $element);
					// Response OK
					return responseOK(NULL, Lang::get('messages.' . $this->sectionTranslate . '.eliminada'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noeliminada'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-e'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrada'));
		}
	}

}
