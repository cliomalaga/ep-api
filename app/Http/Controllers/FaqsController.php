<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\Faq\FaqRepository;
use App\Repositories\Logactivity\LogactivityRepository;
use Lang;

class FaqsController extends Controller {

	protected $faq;
	protected $logactivity;
	protected $section;
	protected $sectionTranslate;

	/*
	 *
	 */
	/* CONSTRUCT */

	public function __construct(FaqRepository $faq, LogactivityRepository $logactivity) {
		$this->faq = $faq;
		$this->logactivity = $logactivity;
		$this->section = 'faqs';
		$this->sectionTranslate = 'pregunta';
	}

	/*
	 *
	 *
	 */
	/* LIST ALL */

	public function index() {
		$elements = $this->faq->allOrderBy('id', 'DESC');
		return responseOK($elements, NULL);
	}

	/*
	 *
	 *
	 */
	/* LIST INDIVIDUAL */

	public function show($id) {
		//If register exist
		if ($element = $this->faq->get($id)) {
			return responseOK($element, NULL);
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrada'));
		}
	}

	/*
	 *
	 *
	 */
	/* LIST INDIVIDUAL FRONT */

	public function showFrontend($path) {
		$data = array();
		// Get id according URL
		$id = pathToId($path);
		// Get item
		$element = $this->faqs->getActive($id);
		return $element;
	}

	/*
	 *
	 *
	 */
	/*
	  /* CREATE */

	public function store(Request $request) {
		//Get params
		$post = $request->all();
		//If not exist image, return fields set NULL
		$image = uploadBase64('image', 'image', $this->section, $post);
		$attached = uploadBase64('attached', 'attached', $this->section, $post);
		//Fill data
		$data = array(
			'active' => parseParam('active', $post),
			'id_user' => \Auth::user()->id,
			'id_subcategory' => parseParam('id_subcategory', $post),
			'title' => parseParam('title', $post),
			'subtitle' => parseParam('subtitle', $post),
			'text' => parseParam('text', $post),
			'image' => parseParam('image', $image),
			'attached' => parseParam('attached', $attached),
			'author' => parseParam('author', $post),
		);
		//Create register
		if ($element = createBD($data, $this->faq)) {
			// Log Activity
			registerLogActivity($this->logactivity, 'create', $this->section, $element);
			// Response OK
			return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.anadida'));
		} else {
			return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noanadida'));
		}
	}

	/*
	 *
	 *
	 */
	/* UPDATE */

	public function update(Request $request, $id) {
		// If register exist
		if ($faq = $this->faq->get($id)) {
			// If user is item owner
			if (isOwn($faq)) {
				//Get params
				$post = $request->all();
				//If not exist image, return fields set NULL
				$image = uploadBase64('image', 'image', $this->section, $post);
				$attached = uploadBase64('attached', 'attached', $this->section, $post);
				//Fill data
				$data = array(
					'active' => parseParam('active', $post),
					'id_user' => \Auth::user()->id,
					'id_subcategory' => parseParam('id_subcategory', $post),
					'title' => parseParam('title', $post),
					'subtitle' => parseParam('subtitle', $post),
					'text' => parseParam('text', $post),
					'image' => parseParam('image', $image),
					'attached' => parseParam('attached', $attached),
					'author' => parseParam('author', $post),
				);
				if (empty($data['id_subcategory'])) {
					$data['id_subcategory'] = null;
				}
				//Update fields
				if ($element = updateBD($id, $data, $this->faq)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'update', $this->section, $element);
					// Response OK
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.actualizada'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noactualizada'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-u'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 *
	 */
	/* ACTIVATE */

	public function activate($id) {
		// If register exist
		if ($faq = $this->faq->get($id)) {
			// If user is item owner
			if (isOwn($faq)) {
				if ($element = updateBD($id, array('active' => 1), $this->faq)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'activate', $this->section, $element);
					// Response OK
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.activada'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noactivada'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-a'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 *
	 */
	/* DESACTIVATE */

	public function desactivate($id) {
		// If register exist
		if ($faq = $this->faq->get($id)) {
			// If user is item owner
			if (isOwn($faq)) {
				if ($element = updateBD($id, array('active' => 0), $this->faq)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'desactivate', $this->section, $element);
					// Response OK
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.desactivada'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.nodesactivada'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-d'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 */
	/* DELETE */

	public function destroy($id) {
		// If register exist
		if ($element = $this->faq->get($id)) {
			// If user is item owner
			if (isOwn($element)) {
				// Delete data
				if ($this->faq->delete($id)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'delete', $this->section, $element);
					// Response OK
					return responseOK(NULL, Lang::get('messages.' . $this->sectionTranslate . '.eliminada'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noeliminada'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-e'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrada'));
		}
	}

}
