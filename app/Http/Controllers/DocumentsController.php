<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Helpers\Helper;
use App\Repositories\Document\DocumentRepository;
use App\Repositories\Logactivity\LogactivityRepository;
use Lang;

class DocumentsController extends Controller {

	protected $document;
	protected $logactivity;
	protected $section;
	protected $sectionTranslate;

	/*
	 *
	 */
	/* CONSTRUCT */

	public function __construct(DocumentRepository $document, LogactivityRepository $logactivity) {
		$this->document = $document;
		$this->logactivity = $logactivity;
		$this->section = 'documents';
		$this->sectionTranslate = 'documento';
	}

	/*
	 *
	 *
	 */
	/* LIST ALL */

	public function index() {
		$elements = $this->document->allOrderBy('id', 'DESC');
		return responseOK($elements, NULL);
	}

	/*
	 *
	 *
	 */
	/* LIST INDIVIDUAL */

	public function show($id) {
		//If register exist
		if ($element = $this->document->get($id)) {
			return responseOK($element, NULL);
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 *
	 */
	/*
	  /* CREATE */

	public function store(Request $request) {
		//Get params
		$post = $request->all();
		//If not exist image, return fields set NULL
		$image = uploadBase64('image', 'image', $this->section, $post);
		$attached = uploadBase64('attached', 'attached', $this->section, $post);
		//Fill data
		$data = array(
			'active' => parseParam('active', $post),
			'id_user' => \Auth::user()->id,
			'id_subcategory' => parseParam('id_subcategory', $post),
			'title' => parseParam('title', $post),
			'subtitle' => parseParam('subtitle', $post),
			'text' => parseParam('text', $post),
			'image' => parseParam('image', $image),
			'attached' => parseParam('attached', $attached),
		);
		//Create register
		if ($element = createBD($data, $this->document)) {
			// Log Activity
			registerLogActivity($this->logactivity, 'create', $this->section, $element);
			// Response OK
			return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.anadido'));
		} else {
			return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noanadido'));
		}
	}

	/*
	 *
	 *
	 */
	/* UPDATE */

	public function update(Request $request, $id) {
		// If register exist
		if ($document = $this->document->get($id)) {
			// If user is item owner
			if (isOwn($document)) {
				//Get params
				$post = $request->all();
				//If not exist image, return fields set NULL
				$image = uploadBase64('image', 'image', $this->section, $post);
				$attached = uploadBase64('attached', 'attached', $this->section, $post);
				//Fill data
				$data = array(
					'active' => parseParam('active', $post),
					'id_subcategory' => parseParam('id_subcategory', $post),
					'title' => parseParam('title', $post),
					'subtitle' => parseParam('subtitle', $post),
					'text' => parseParam('text', $post),
					'image' => parseParam('image', $image),
					'attached' => parseParam('attached', $attached),
				);
				if (empty($data['id_subcategory'])) {
					$data['id_subcategory'] = null;
				}
				//Update fields
				if ($element = updateBD($id, $data, $this->document)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'update', $this->section, $element);
					// Response OK
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.actualizado'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noactualizado'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-u'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 *
	 */
	/* ACTIVATE */

	public function activate($id) {
		// If register exist
		if ($document = $this->document->get($id)) {
			// If user is item owner
			if (isOwn($document)) {
				if ($element = updateBD($id, array('active' => 1), $this->document)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'activate', $this->section, $element);
					// Response OK
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.activado'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noactivado'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-a'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 *
	 */
	/* DESACTIVATE */

	public function desactivate($id) {
		// If register exist
		if ($document = $this->document->get($id)) {
			// If user is item owner
			if (isOwn($document)) {
				if ($element = updateBD($id, array('active' => 0), $this->document)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'desactivate', $this->section, $element);
					// Response OK
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.desactivado'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.nodesactivado'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-d'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 */
	/* DELETE */

	public function destroy($id) {
		// If register exist
		if ($element = $this->document->get($id)) {
			// If user is item owner
			if (isOwn($element)) {
				// Delete data
				if ($this->document->delete($id)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'delete', $this->section, $element);
					// Response OK
					return responseOK(NULL, Lang::get('messages.' . $this->sectionTranslate . '.eliminado'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noeliminado'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-e'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

}
