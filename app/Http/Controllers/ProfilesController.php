<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\Profile\ProfileRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\Roles\RolesRepository;
use App\Repositories\Configuration\ConfigurationRepository;
use App\Repositories\Logactivity\LogactivityRepository;
use User;
use Lang;

class ProfilesController extends Controller {

	protected $profile;
	protected $user;
	protected $roles;
	protected $configuration;
	protected $logactivity;
	protected $section;
	protected $sectionTranslate;

	/*
	 *
	 */
	/* CONSTRUCT */

	public function __construct(ProfileRepository $profile, UserRepository $user, RolesRepository $roles, ConfigurationRepository $configuration, LogactivityRepository $logactivity) {
		$this->profile = $profile;
		$this->user = $user;
		$this->roles = $roles;
		$this->configuration = $configuration;
		$this->logactivity = $logactivity;
		$this->section = 'profiles';
		$this->sectionTranslate = 'perfil';
	}

	/*
	 *
	 *
	 */
	/* LIST ALL */

	public function indexUser() {
		$array = array();
		$elements = $this->profile->allOrderBy('id', 'DESC');
		foreach ($elements as $element) {
			$element = $this->parseFields($element);
			// If element is not softdeleted
			if (empty($element->user->deleted_at)) {
				array_push($array, $element);
			}
		}

		return responseOK($array, NULL);
	}

	/*
	 *
	 *
	 */
	/* LIST INDIVIDUAL */

	public function show() {
		//If register exist
		if ($element = $this->profile->getByUser(\Auth::user()->id)) {
			// If element is not softdeleted
			if (empty($element->user->deleted_at)) {
				$element = $this->parseFields($element);
				return responseOK($element, NULL);
			} else {
				return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 *
	 */
	/* LIST USER INDIVIDUAL */

	public function showUser($id) {
		//If register exist
		if ($element = $this->profile->get($id)) {
			// If element is not softdeleted
			if (empty($element->user->deleted_at)) {
				$element = $this->parseFields($element);
				return responseOK($element, NULL);
			} else {
				return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 *
	 */
	/*
	  /* CREATE */

	public function storeUser(Request $request) {
		// If register not exist
		if (!$this->profile->getByUser($request['id_user'])) {
			//Get params
			$post = $request->all();
			//Check if exist email and is valid
			if (!empty($post['email']) && isMailValid($post['email'])) {
				//Update password field of new user
				if ($status = $this->createPassword($post, $this->user)) {
					//If create password is OK
					if ($status['status'] == 'OK') {
						//Fill data, password is empty by default, update later
						$data = array(
							'email' => parseParam('email', $post),
							'password' => $status['password'],
						);
						//Create user
						if ($elementUser = createBD($data, $this->user)) {
							//If not exist image, return fields set NULL
							$image = uploadBase64('image', 'avatar', $this->section, $post);
							//If id_rol is empty, by default assign last rol
							if (empty($post['id_rol'])) {
								$id_rol = $this->roles->lastRol()->id;
							} else {
								$id_rol = parseParam('id_rol', $post);
							}
							//Fill data profile
							$data = array(
								'id_user' => $elementUser['id'],
								'id_rol' => (string) $id_rol,
								'name' => parseParam('name', $post),
								'lastname' => parseParam('lastname', $post),
								'about' => parseParam('about', $post),
								'avatar' => parseParam('image', $image),
								'phone' => parseParam('phone', $post),
								'email' => parseParam('email', $post),
								'Facebook' => parseParam('Facebook', $post),
								'Twitter' => parseParam('Twitter', $post),
								'Linkedin' => parseParam('Linkedin', $post),
								'Google' => parseParam('Google', $post),
								'active' => parseParam('active', $post),
							);
							//Create profile to user
							if ($elementProfile = createBD($data, $this->profile)) {
								$elementProfile = $this->parseFields($elementProfile);
								//Fill data configuration
								$data = array(
									'id_user' => $elementUser['id'],
									'animations' => 1,
									'theme_basic' => 0,
									'color_primary' => '#303641',
									'delay_notification' => 5,
									'delay_locked' => 30,
									'view' => 'grid',
									'view_calendar' => 'month',
								);
								//Create configuration to user
								if ($element = createBD($data, $this->configuration)) {
									// Log Activity
									registerLogActivity($this->logactivity, 'create', $this->section, $elementProfile);
									// Response OK
									return responseOK($elementProfile, Lang::get('messages.' . $this->sectionTranslate . '.anadido'));
								} else {
									$this->user->delete($elementUser['id']);
									$this->profile->delete($elementProfile['id']);

									return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noanadida'));
								}
							} else {
								$this->user->delete($elementUser['id']);
								return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noanadido'));
							}
						} else {
							return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.' . $status['type']));
						}
					} else {
						return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.' . $status['type']));
					}
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noanadido'));
				}
			} else {
				return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.faltaemail'));
			}
		} else {
			return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.yaexiste'));
		}
	}

	/*
	 *
	 *
	 */
	/* UPDATE */

	public function update(Request $request) {
		// If register exist
		if ($this->profile->getByUser(\Auth::user()->id)) {
			//Get params
			$post = $request->all();
			//If not exist image, return fields set NULL
			$image = uploadBase64('image', 'avatar', $this->section, $post);

			//Fill data
			$data = array(
				'name' => parseParam('name', $post),
				'lastname' => parseParam('lastname', $post),
				'about' => parseParam('about', $post),
				'avatar' => parseParam('image', $image),
				'phone' => parseParam('phone', $post),
				'email' => parseParam('email', $post),
				'Facebook' => parseParam('Facebook', $post),
				'Twitter' => parseParam('Twitter', $post),
				'Linkedin' => parseParam('Linkedin', $post),
				'Google' => parseParam('Google', $post),
			);
			//Update password field if is necesary
			if ($status = $this->updatePassword(\Auth::user()->id, $post, $this->user, true)) {
				if ($status['status'] == 'OK') {
					//Update fields
					if ($element = updateBD(\Auth::user()->profile->id, $data, $this->profile)) {
						// Log Activity
						registerLogActivity($this->logactivity, 'update', $this->section, $element);
						// Response OK
						$element = $this->parseFields($element);
						return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.actualizado'));
					} else {
						return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noactualizado'));
					}
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.' . $status['type']));
				}
			} else {
				return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.' . $status['type']));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 *
	 */
	/* UPDATE USER */

	public function updateUser(Request $request, $id) {
		// If user is superuser
		if (isSuperuser()) {
			// If register exist
			if ($profile = $this->profile->get($id)) {
				//Get params
				$post = $request->all();
				//If not exist image, return fields set NULL
				$image = uploadBase64('image', 'avatar', $this->section, $post);

				//Fill data
				$data = array(
					'id_rol' => parseParam('id_rol', $post),
					'name' => parseParam('name', $post),
					'lastname' => parseParam('lastname', $post),
					'about' => parseParam('about', $post),
					'avatar' => parseParam('image', $image),
					'phone' => parseParam('phone', $post),
					'email' => parseParam('email', $post),
					'Facebook' => parseParam('Facebook', $post),
					'Twitter' => parseParam('Twitter', $post),
					'Linkedin' => parseParam('Linkedin', $post),
					'Google' => parseParam('Google', $post),
					'active' => parseParam('active', $post),
				);
				// If user to edit is same that authenticate user, is imposible desactivate user
				if ($id == \Auth::user()->id) {
					unset($data['active']);
				}
				//Update password field if is necesary
				if ($status = $this->updatePassword($profile->id_user, $post, $this->user, false)) {
					if ($status['status'] == 'OK') {
						//Update fields
						if ($element = updateBD($id, $data, $this->profile)) {
							// Log Activity
							registerLogActivity($this->logactivity, 'update', $this->section, $element);
							// Response OK
							$element = $this->parseFields($element);
							return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.actualizado'));
						} else {
							return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noactualizado'));
						}
					} else {
						return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.' . $status['type']));
					}
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.' . $status['type']));
				}
			} else {
				return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
			}
		} else {
			return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-u'));
		}
	}

	public function createPassword($post, $repository) {
		if (!empty($post['pnew'] && !empty($post['pmatch']))) {
			// If two password field have matched, we updated DB
			if ($post['pnew'] == $post['pmatch']) {
				//Fill data
				$status = array(
					'status' => 'OK',
					'password' => \Hash::make($post['pnew'])
				);
			} else {
				$status = array(
					'status' => 'KO',
					'type' => 'nocoinciden'
				);
			}
		} else {
			$status = array(
				'status' => 'KO',
				'type' => 'nocreado-faltancampos'
			);
		}
		return $status;
	}

	public function updatePassword($id, $post, $repository, $checkCurrent) {
		if (empty($post['pcurrent']) && empty($post['pmatch']) && empty($post['pnew'])) {
			$status = array(
				'status' => 'OK',
				'type' => ''
			);
		} else if (/**/ ($checkCurrent && empty($post['pcurrent'])) ||
			/**/ (!empty($post['pnew']) && empty($post['pmatch'])) ||
			/**/ (empty($post['pnew']) && !empty($post['pmatch']))
		) {
			$status = array(
				'status' => 'KO',
				'type' => 'faltancampos'
			);
		} else {
			$user = $this->user->getById($id);
			// If current password if not is necessary or current password have matched on DB
			if (!$checkCurrent || (\Hash::check($post['pcurrent'], $user->password))) {
				// If two password field have matched, we updated DB
				if ($post['pnew'] == $post['pmatch']) {
					//Fill data
					$data['password'] = \Hash::make($post['pnew']);
					if ($element = updateBD($id, $data, $repository)) {
						$status = array(
							'status' => 'OK',
						);
					} else {
						$status = array(
							'status' => 'KO',
							'type' => 'noactualizado'
						);
					}
				} else {
					$status = array(
						'status' => 'KO',
						'type' => 'nocoinciden'
					);
				}
			} else {
				$status = array(
					'status' => 'KO',
					'type' => 'noactual'
				);
			}
		}
		return $status;
	}

	/*
	 *
	 *
	 */
	/* ACTIVATE */

	public function activate($id) {
		if (isSuperuser()) {
			if ($id == \Auth::user()->id) {
				return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.activarpropio'));
			} else {
				if ($element = updateBD($id, array('active' => 1), $this->profile)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'activate', $this->section, $element);
					// Response OK
					$element = $this->parseFields($element);
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.activado'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noactivado'));
				}
			}
		} else {
			return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-a'));
		}
	}

	/*
	 *
	 *
	 */
	/* DESACTIVATE */

	public function desactivate($id) {
		if (isSuperuser()) {
			if ($id == \Auth::user()->id) {
				return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.desactivarpropio'));
			} else {
				if ($element = updateBD($id, array('active' => 0), $this->profile)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'desactivate', $this->section, $element);
					// Response OK
					$element = $this->parseFields($element);
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.desactivado'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.nodesactivado'));
				}
			}
		} else {
			return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-d'));
		}
	}

	/*
	 *
	 */
	/* DELETE */

	public function destroy($id) {
		if (isSuperuser()) {
			if ($id == \Auth::user()->id) {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.eliminarpropio'));
			} else {
				// If register exist
				if ($profile = $this->profile->get($id)) {
					$id_user = $profile->id_user;
					// Delete Profile from user
//					if ($this->profile->delete($id)) {
					// Delete Configuration from user
//						if ($this->configuration->deleteByUser($id_user)) {
					// Delete User
					if ($this->user->delete($id_user)) {
						// Log Activity
						registerLogActivity($this->logactivity, 'delete', $this->section, $profile);
						return responseOK(NULL, Lang::get('messages.' . $this->sectionTranslate . '.eliminado'));
					} else {
						return responseKO('505', Lang::get('messages.' . $this->sectionTranslate . '.noeliminado'));
					}
//						} else {
//							return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noeliminado'));
//						}
//					} else {
//						return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noeliminado'));
//					}
				} else {
					return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
				}
			}
		} else {
			return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-e'));
		}
	}

	/*
	 *
	 *
	 */
	/* PARSE FIELD BEFORE SEND TO FRONT */

	public function parseFields($element) {
		// If not exist profile to user, return null
		if (!empty($element->user->email)) {
			$element->email = $element->user->email;
		}
		return $element;
	}

}
