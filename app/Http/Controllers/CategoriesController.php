<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Subcategory\SubcategoryRepository;
use App\Repositories\Logactivity\LogactivityRepository;
use Lang;

class CategoriesController extends Controller {

	protected $category;
	protected $subcategory;
	protected $logactivity;
	protected $section;
	protected $sectionTranslate;

	/*
	 *
	 */
	/* CONSTRUCT */

	public function __construct(CategoryRepository $category, SubcategoryRepository $subcategory, LogactivityRepository $logactivity) {
		$this->category = $category;
		$this->subcategory = $subcategory;
		$this->logactivity = $logactivity;
		$this->section = 'subcategories';
		$this->sectionTranslate = 'subcategoria';
	}

	/*
	 *
	 *
	 */
	/* LIST ALL CATEGORIES */

	public function indexCategories() {
		$elements = $this->category->allOrderBy('name', 'ASC');
		return responseOK($elements, NULL);
	}

	/*
	 *
	 *
	 */
	/* LIST ALL SUBCATEGORIES */

	public function indexSubcategories() {
		$elements = $this->subcategory->allOrderBy('name', 'ASC');
		foreach ($elements as $element) {
			$element = $this->parseFields($element);
		}
		return responseOK($elements, NULL);
	}

	/*
	 *
	 *
	 */
	/*
	  /* CREATE */

	public function store(Request $request) {
		//Get params
		$post = $request->all();
		//Fill data
		$data = array(
			'id_category' => parseParam('id_category', $post),
			'name' => parseParam('name', $post),
		);
		if (!empty($data['id_category'])) {
			if (!empty($data['name'])) {
				//Create register
				if ($element = createBD($data, $this->subcategory)) {
					$element = $this->parseFields($element);
					// Response OK
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.anadida'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noanadida'));
				}
			} else {
				return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.nonombre'));
			}
		} else {
			return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.nocategoria'));
		}
	}

	/*
	 *
	 *
	 */
	/* UPDATE */

	public function update(Request $request, $id) {
		// If register exist
		if ($subcategory = $this->subcategory->get($id)) {
			//Get params
			$post = $request->all();
			//Fill data
			$data = array(
				'name' => parseParam('name', $post),
			);
			if (!empty($data['name'])) {
				//Update fields
				if ($element = updateBD($id, $data, $this->subcategory)) {
					$element = $this->parseFields($element);
					// Response OK
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.actualizada'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noactualizada'));
				}
			} else {
				return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.nonombre'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrado'));
		}
	}

	/*
	 *
	 */
	/* DELETE */

	public function destroy($id) {
		// If register exist
		if ($element = $this->subcategory->get($id)) {
			// Delete data
			if ($this->subcategory->delete($id)) {
				// Response OK
				return responseOK(NULL, Lang::get('messages.' . $this->sectionTranslate . '.eliminada'));
			} else {
				return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noeliminada'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrada'));
		}
	}

	/*
	 *
	 *
	 */
	/* PARSE FIELD BEFORE SEND TO FRONT */

	public function parseFields($element) {
		$element->category_name = $element->category->name;
		$element->id_category = (int) $element->id_category;

		return $element;
	}

}
