<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\Subcategory\SubcategoryRepository;
use App\Repositories\Logactivity\LogactivityRepository;
use Lang;

class SubcategoriesController extends Controller {

	protected $subcategory;
	protected $logactivity;
	protected $section;
	protected $sectionTranslate;

	/*
	 *
	 */
	/* CONSTRUCT */

	public function __construct(SubcategoryRepository $subcategory, LogactivityRepository $logactivity) {
		$this->subcategory = $subcategory;
		$this->logactivity = $logactivity;
		$this->section = 'subcategories';
		$this->sectionTranslate = 'subcategoria';
	}

	/*
	 *
	 *
	 */
	/* LIST ALL */

	public function index() {
		$elements = $this->subcategory->allOrderBy('id', 'DESC');
		foreach ($elements as $element) {
			$element = $this->parseFields($element);
		}
		return responseOK($elements, NULL);
	}

	/*
	 *
	 *
	 */
	/* PARSE FIELD BEFORE SEND TO FRONT */

	public function parseFields($element) {
		$element->category_name = $element->category->name;
		return $element;
	}

}
