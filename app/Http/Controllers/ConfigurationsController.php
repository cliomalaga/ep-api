<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\Configuration\ConfigurationRepository;
use App\Repositories\Logactivity\LogactivityRepository;
use Lang;

class ConfigurationsController extends Controller {

	protected $configuration;
	protected $logactivity;
	protected $section;
	protected $sectionTranslate;

	/*
	 *
	 */
	/* CONSTRUCT */

	public function __construct(ConfigurationRepository $configuration, LogactivityRepository $logactivity) {
		$this->configuration = $configuration;
		$this->logactivity = $logactivity;
		$this->section = 'configurations';
		$this->sectionTranslate = 'configuraciones';
	}

	/*
	 *
	 *
	 */
	/* LIST INDIVIDUAL */

	public function show() {
		//If register exist
		if ($element = $this->configuration->getOwn()) {
			$element = $this->parseFields($element);
			return responseOK($element, NULL);
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrada'));
		}
	}

	/*
	 *
	 *
	 */
	/* UPDATE */

	public function update(Request $request, $id) {
		// If register exist
		if ($configuration = $this->configuration->get($id)) {
			// If user is item owner
			if (isOwn($configuration)) {
				//Get params
				$post = $request->all();
				//Fill data
				$data = array(
					'id_user' => \Auth::user()->id,
					'animations' => parseParam('animations', $post),
					'theme_basic' => parseParam('theme_basic', $post),
					'color_primary' => parseParam('color_primary', $post),
					'delay_notification' => parseParam('delay_notification', $post),
					'delay_locked' => parseParam('delay_locked', $post),
					'view' => parseParam('view', $post),
					'view_calendar' => parseParam('view_calendar', $post),
				);
				//Update fields
				if ($element = updateBD($id, $data, $this->configuration)) {
					// Log Activity
					registerLogActivity($this->logactivity, 'update', $this->section, $element);
					// Response OK
					$element = $this->parseFields($element);
					return responseOK($element, Lang::get('messages.' . $this->sectionTranslate . '.actualizada'));
				} else {
					return responseKO('500', Lang::get('messages.' . $this->sectionTranslate . '.noactualizada'));
				}
			} else {
				return responseKO('401', Lang::get('messages.' . $this->sectionTranslate . '.nopropietario-u'));
			}
		} else {
			return responseKO('404', Lang::get('messages.' . $this->sectionTranslate . '.noencontrada'));
		}
	}

	/*
	 *
	 *
	 */
	/* PARSE FIELD BEFORE SEND TO FRONT */

	public function parseFields($element) {
		$element->created_by = $element->user->email;
		$element->animations = (boolean) $element->animations;
		$element->theme_basic = (boolean) $element->theme_basic;
		return $element;
	}

}
