<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;
use App\Repositories\User\UserRepository;
use App\User;
use App\Profile;
use Lang;

class AuthenticateController extends Controller {

	protected $user;

	public function __construct(UserRepository $user) {
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
//		$this->middleware('jwt.auth', ['except' => ['authenticate']]);
		$this->user = $user;
	}

	public function authenticate(Request $request) {
		$credentials = $request->only('email', 'password');

//		if (!empty($credentials['email']) && !empty($credentials['password'])) {
		if (empty($credentials['email']) && empty($credentials['password'])) {
			$credentials = [
				'email' => $request->header('email'),
				'password' => $request->header('password')];
		}
		try {
			// verify the credentials and create a token for the user
			if (!$token = JWTAuth::attempt($credentials)) {
				return responseKO('401', Lang::get('messages.login.nocredenciales'));
			}
		} catch (JWTException $e) {
			// something went wrong
			return responseKO('500', Lang::get('messages.pagina.notoken'));
		}
		$user = $this->user->getByEmail($credentials['email']);
		if ($user->profile && $user->profile->active == 1) {
			$response = [
				'token' => $token,
				'user' => $user
			];
			return responseOK($response, NULL);
		} else {
			return responseKO('401', Lang::get('messages.login.noactivo'));
		}
	}

}
