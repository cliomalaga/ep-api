<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Lang;
use \Carbon\Carbon;

class LangMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		if (!empty(session('lang'))) {
			App::setLocale(session('lang'));
		}
		Carbon::setLocale(Lang::getLocale());
		return $next($request);
	}

}
