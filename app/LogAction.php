<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogAction extends Model {

	protected $table = 'logactions';
	public $timestamps = false;
	protected $fillable = [
		'name',
	];

}
