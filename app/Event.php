<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {

	protected $table = 'events';
	public $timestamps = true;
	protected $fillable = [
		'id_user',
		'description',
		'image',
		'datetime_start',
		'datetime_end',
		'active',
	];

	public function user() {
		return $this->belongsTo('App\User', 'id_user')->withTrashed();
		;
	}

}
