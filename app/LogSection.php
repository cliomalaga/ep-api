<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogSection extends Model {

	protected $table = 'logsections';
	public $timestamps = false;
	protected $fillable = [
		'name',
	];

}
