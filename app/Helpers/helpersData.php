<?php

/*
 *
 */
/* CHECK SUPERUSER */

function isSuperuser() {
	if (\Auth::user()->profile->id_rol <= 1) {
		return true;
	} else {
		return false;
	}
}

/*
 *
 */
/* CHECK ADMIN */

function isAdmin() {
	if (\Auth::user()->profile->id_rol <= 2) {
		return true;
	} else {
		return false;
	}
}

/*
 *
 */
/* CHECKS OWNER */

function isOwn($post) {
	if (!empty($post['id_user'])) {
		if (\Auth::user()->profile->id_rol <= 1 || \Auth::user()->id == $post['id_user']) {
			return true;
		} else {
			return false;
		}
	} else {
		return true;
	}
}

/*
 *
 */
/* UPLOADS */

function uploadBase64($type, $input, $destinationPath, $post) {
	//Initialize data reponse
	$data = array(
		'image' => NULL,
	);
	if (($type == 'image' && !empty($post['images'])) ||
		($type == 'attached' && !empty($post['attacheds']))) {
		//
		// Set path and file
		$basePath = $type . 's/';
		$file = $post[$type . 's'];
		//Set path and name to upload
		$str = $file['data'];
		list(, $str) = explode(';', $str);
		list(, $str) = explode(',', $str);
		$base64 = base64_decode($str);
//		$url_server = '/home/evaluacigu/www/dashboard/';
		$url_server = '/xampp/htdocs/EP-Dashboard/';
		$url_relative = 'public/' . $basePath . $destinationPath . '/';
		$url = $url_server . $url_relative;
		// Create dir if not exist
		if (!file_exists($url)) {
			mkdir($url, 0777, true);
		}
		// Set name and upload file
		$name = date('Ymd') . '_' . $file['name'];
		file_put_contents($url . $name, $base64);
		$data[$type] = $url_relative . $name;
		//
	} else {
		if (($type == 'image' && empty($post['image']) && empty($post['avatar'])) ||
			($type == 'attached' && empty($post['attached']))) {
			$data = array(
				$type => " ",
			);
		}
	}
	return $data;
}

/*
 *
 */
/* CREATES */

function createBD($data, $repository) {
//Unset fields with NULL value
	$data = unsetNullFields($data);
//Update data
	if ($element = $repository->insert($data)) {
		return $element;
	} else {
		return null;
	}
}

/*
 *
 */
/* UPDATES */

function updateBD($id, $data, $repository) {
//Unset fields with NULL value
	$data = unsetNullFields($data);
//Update data
	if ($repository->update($id, $data)) {
		$element = $repository->get($id);
		return $element;
	} else {
		return null;
	}
}

/*
 *
 */
/* LOG ACTIVITY */

function actionToId($action) {
	$id = null;
	switch ($action) {
		case 'create': $id = 1;
			break;
		case 'update':$id = 2;
			break;
		case 'delete':$id = 3;
			break;
		case 'activate':$id = 4;
			break;
		case 'desactivate':$id = 5;
			break;
		case 'send':$id = 6;
			break;
	}
	return $id;
}

function sectionToId($section) {
	$id = null;
	switch ($section) {
		case 'configurations': $id = 1;
			break;
		case 'profiles': $id = 2;
			break;
		case 'users': $id = 3;
			break;
		case 'emails': $id = 4;
			break;
		case 'events': $id = 5;
			break;
		case 'tests': $id = 6;
			break;
		case 'faqs': $id = 7;
			break;
		case 'documents': $id = 8;
			break;
		case 'teachings': $id = 9;
			break;
		case 'casees': $id = 10;
			break;
		case 'investigations': $id = 11;
			break;
	}
	return $id;
}

function idToSection($id) {
	$section = null;
	switch ($id) {
		case '1': $section = 'configuracion';
			break;
		case '2': $section = 'perfil';
			break;
		case '3': $section = 'usuario';
			break;
		case '4': $section = 'mensaje';
			break;
		case '5': $section = 'evento';
			break;
		case '6': $section = 'prueba';
			break;
		case '7': $section = 'pregunta';
			break;
		case '8': $section = 'documento';
			break;
		case '9': $section = 'enseñanza';
			break;
		case '10': $section = 'caso';
			break;
		case '11': $section = 'investigación';
			break;
	}
	return $section;
}

function registerLogActivity($logRepository, $action, $section, $element) {
	$dataLogActivity = [
		'id_user' => \Auth::user()->id,
		'id_user_original' => $element->id_user,
		'id_logaction' => actionToId($action),
		'id_logsection' => sectionToId($section),
		'id_item' => $element->id,
	];

	createBD($dataLogActivity, $logRepository);
}
