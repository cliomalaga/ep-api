<?php

/*
 *
 */
/* PARSE RESPONSES */

function parseParam($field, $data) {
	if (isset($data[$field])) {
		if (is_string($data[$field])) {
			return trim($data[$field]);
		} else {
			return $data[$field];
		}
	}
//	} else {
//		return NULL;
//	}
}

/*
 *
 *
 */

function unsetNullFields($data) {
	foreach ($data as $field => $value) {
		// If field is a date, we can set null
		if ($field != "date" && $field != "date_start" && $field != "date_end" && $field != "id_subcategory") {
			if (is_null($value)) {
				unset($data[$field]);
			}
		}
	}
	return $data;
}

/*
 *
 */
/* RESPONSES */

function responseOK($response, $message) {
	return responseJSON(true, '200', $response, $message);
}

/*
 *
 */

function responseKO($code, $message) {
	return responseJSON(false, $code, NULL, $message);
}

/*
 *
 */

function responseJSON($ok, $code, $response, $message) {
	$data = array(
		'ok' => $ok,
		'code' => $code,
		'message' => $message,
		'response' => $response
	);
	$header = array(
		'Content-Type' => 'application/json; charset=UTF-8',
		'charset' => 'utf-8'
	);
	return Response::json($data, '200', $header, JSON_UNESCAPED_UNICODE);
}
