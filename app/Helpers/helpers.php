<?php

function logConsole($data = NULL) {
	$isevaled = false;
	$data = json_encode($data);

	# sanitalize
	$data = $data ? $data : '';
	$search_array = array("#'#", '#""#', "#''#", "#\n#", "#\r\n#");
	$replace_array = array('"', '', '', '\\n', '\\n');
	$data = preg_replace($search_array, $replace_array, $data);
	$data = ltrim(rtrim($data, '"'), '"');
	$data = $isevaled ? $data : ($data[0] === "'") ? $data : "'" . $data . "'";

//	$js = <<<JSCODE
//\n<script>
// // fallback - to deal with IE (or browsers that don't have console)
// if (! window.console) console = {};
// console.log = console.log || function(name, data){};
// // end of fallback
//
// console.log($data);
//</script>
//JSCODE;
	echo $data;
}

function isMailValid($email) {
	if (\Validator::make(['email' => $email], ['email' => 'required|email'])->fails()) {
		return false;
	} else {
		return true;
	}
}

function correctDateToDB($date) {
	// d/M/Y  <==>  Y/M/d
	if ($date) {
		$pos = strpos($date, '/');
		if ($pos === false) {
			$arrayDate = explode('-', $date);
		} else {
			$arrayDate = explode('/', $date);
		}
		$day = $arrayDate[0];
		$month = $arrayDate[1];
		$year = $arrayDate[2];
		return $year . '-' . $month . '-' . $day;
	}
	return null;
}

function correctDateTimeToDB($dateTime, $time) {
	// d/M/Y H:m  <==>  Y/M/d H:m
	if ($dateTime) {
		$arrayDateTime = explode(' ', $dateTime);
		$date = $arrayDateTime[0];
		$pos = strpos($date, '/');
		if ($pos === false) {
			$arrayDate = explode('-', $date);
		} else {
			$arrayDate = explode('/', $date);
		}
		$day = $arrayDate[0];
		$month = $arrayDate[1];
		$year = $arrayDate[2];
		if (!empty($time)) {
			$str = $year . '-' . $month . '-' . $day . ' ' . $time;
		} else if (!empty($arrayDateTime[1])) {
			$str = $year . '-' . $month . '-' . $day . ' ' . $arrayDateTime[1];
		} else {
			$str = $year . '-' . $month . '-' . $day . ' 00:00';
		}
		return $str;
	}
	return null;
}

function correctDateToFront($date) {
	// M/d/Y  <==>  d/M/Y
	if ($date) {
		$pos = strpos($date, '/');
		if ($pos === false) {
			$arrayDate = explode('-', $date);
		} else {
			$arrayDate = explode('/', $date);
		}
		$day = $arrayDate[2];
		$month = $arrayDate[1];
		$year = $arrayDate[0];
		return $day . '/' . $month . '/' . $year;
	}
	return null;
}

function correctDateTimeToFront($dateTime) {
	// M/d/Y H:m:s  <==>  d/M/Y H:m
	if ($dateTime) {
		$arrayDateTime = explode(' ', $dateTime);
		$date = $arrayDateTime[0];
		$time = $arrayDateTime[1];
		$pos = strpos($date, '/');
		if ($pos === false) {
			$arrayDate = explode('-', $date);
		} else {
			$arrayDate = explode('/', $date);
		}
		$day = $arrayDate[2];
		$month = $arrayDate[1];
		$year = $arrayDate[0];
		$arrayTime = explode(':', $time);
		return $day . '/' . $month . '/' . $year . ' ' . $arrayTime[0] . ':' . $arrayTime[1];
	}
	return null;
}

function parseTimeToFront($dateTime) {
	// M/d/Y H:m:s  <==>  H:m
	if ($dateTime) {
		$arrayDateTime = explode(' ', $dateTime);
		$time = $arrayDateTime[1];
		$arrayTime = explode(':', $time);
		return $arrayTime[0] . ':' . $arrayTime[1];
	}
	return null;
}

// Get ID item according Url
function pathToId($url) {
	$urlArray = explode('-', $url);
	$id = (int) end($urlArray);
	return $id;
}
