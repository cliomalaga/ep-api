<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */
Route::pattern('id', '[0-9]+');

Auth::routes();

// LOGIN
Route::post('authenticate', 'AuthenticateController@authenticate');


// FRONTEND
Route::group(['prefix' => 'EP'], function() {
	// Login frontend
	Route::post('authenticate', 'AuthenticateController@authenticate');
	// Home
	Route::get('/menu', 'FrontendController@getMenu');
	Route::get('/items-home', 'FrontendController@getItemsHome');
	Route::get('/preguntas-home', 'FrontendController@getFaqsHome');
	Route::get('/vistos', 'FrontendController@getRandomHome');
	// Sidebar
	Route::post('/archivo', 'FrontendController@getArchive');
	Route::get('/ultima', 'FrontendController@getLast');
	Route::post('/ultimas', 'FrontendController@getTimeline');
	Route::get('/galeria', 'FrontendController@getGallery');
	Route::post('/misma-categoria', 'FrontendController@getSameCategory');
	Route::post('/misma-subcategoria', 'FrontendController@getSameSubcategory');
	// Mails
	Route::post('enviar-mensaje', 'FrontendController@sendEmail');
	// Search
	Route::post('buscar', 'FrontendController@search');
	// Category
	Route::get('biblioteca/{category}', 'FrontendController@getCategoryArchive')
		->where('category', 'pruebas|preguntas|documentos|ensenanzas|casos|investigaciones');
	Route::get('{category}', 'FrontendController@getCategory')
		->where('category', 'pruebas|preguntas|documentos|ensenanzas|casos|investigaciones');
	// Subcategory
	Route::get('biblioteca/{category}/{subcategory}', 'FrontendController@getSubcategoryArchive')
		->where('category', 'pruebas|preguntas|documentos|ensenanzas|casos|investigaciones');
	Route::get('{category}/{subcategory}', 'FrontendController@getSubcategory')
		->where('category', 'pruebas|preguntas|documentos|ensenanzas|casos|investigaciones');
	// Item
	Route::get('{category}/{subcategory}/{path}', 'FrontendController@showFrontend')
		->where('category', 'pruebas|preguntas|documentos|ensenanzas|casos|investigaciones');
	// Teacher
	Route::get('profesores', 'FrontendController@getTeachers');
	Route::get('profesores/{path}', 'FrontendController@getTeacher');
});


// API
Route::group(array('middleware' => 'jwt.auth'), function() {

	//	Route::auth();
	// LANGUAGES
	Route::get('lang/{lang}', function ($lang) {
		session(['lang' => $lang]);
		return \Redirect::back();
	})->where([
		'lang' => 'en|es'
	]);

	// ROLES
	Route::get('/roles', 'RolesController@index');
	// PROFILES
	Route::get('/perfil', 'ProfilesController@show');
	Route::put('/perfil', 'ProfilesController@update');
	// CATEGORIES
	Route::get('/categorias', 'CategoriesController@indexCategories');
	// SUBCATEGORIES
	Route::get('/subcategorias', 'CategoriesController@indexSubcategories');
	Route::post('/subcategorias', 'CategoriesController@store');
	Route::put('/subcategorias/{id}', 'CategoriesController@update');
	Route::delete('/subcategorias/{id}', 'CategoriesController@destroy');
	// TESTS
	Route::get('/pruebas', 'TestsController@index');
	Route::get('/pruebas/{id}', 'TestsController@show');
	Route::post('/pruebas', 'TestsController@store');
	Route::get('/pruebas/{id}/activar', 'TestsController@activate');
	Route::get('/pruebas/{id}/desactivar', 'TestsController@desactivate');
	Route::put('/pruebas/{id}', 'TestsController@update');
	Route::delete('/pruebas/{id}', 'TestsController@destroy');
	// FAQS
	Route::get('/preguntas', 'FaqsController@index');
	Route::get('/preguntas/{id}', 'FaqsController@show');
	Route::post('/preguntas', 'FaqsController@store');
	Route::put('/preguntas/{id}', 'FaqsController@update');
	Route::get('/preguntas/{id}/activar', 'FaqsController@activate');
	Route::get('/preguntas/{id}/desactivar', 'FaqsController@desactivate');
	Route::delete('/preguntas/{id}', 'FaqsController@destroy');
	// DOCUMENTS
	Route::get('/documentos', 'DocumentsController@index');
	Route::get('/documentos/{id}', 'DocumentsController@show');
	Route::post('/documentos', 'DocumentsController@store');
	Route::put('/documentos/{id}', 'DocumentsController@update');
	Route::get('/documentos/{id}/activar', 'DocumentsController@activate');
	Route::get('/documentos/{id}/desactivar', 'DocumentsController@desactivate');
	Route::delete('/documentos/{id}', 'DocumentsController@destroy');
	// TEACHINGS
	Route::get('/ensenanzas', 'TeachingsController@index');
	Route::get('/ensenanzas/{id}', 'TeachingsController@show');
	Route::post('/ensenanzas', 'TeachingsController@store');
	Route::put('/ensenanzas/{id}', 'TeachingsController@update');
	Route::get('/ensenanzas/{id}/activar', 'TeachingsController@activate');
	Route::get('/ensenanzas/{id}/desactivar', 'TeachingsController@desactivate');
	Route::delete('/ensenanzas/{id}', 'TeachingsController@destroy');
	// CASES
	Route::get('/casos', 'CaseesController@index');
	Route::get('/casos/{id}', 'CaseesController@show');
	Route::post('/casos', 'CaseesController@store');
	Route::put('/casos/{id}', 'CaseesController@update');
	Route::get('/casos/{id}/activar', 'CaseesController@activate');
	Route::get('/casos/{id}/desactivar', 'CaseesController@desactivate');
	Route::delete('/casos/{id}', 'CaseesController@destroy');
	// INVESTIGATIONS
	Route::get('/investigaciones', 'InvestigationsController@index');
	Route::get('/investigaciones/{id}', 'InvestigationsController@show');
	Route::post('/investigaciones', 'InvestigationsController@store');
	Route::put('/investigaciones/{id}', 'InvestigationsController@update');
	Route::get('/investigaciones/{id}/activar', 'InvestigationsController@activate');
	Route::get('/investigaciones/{id}/desactivar', 'InvestigationsController@desactivate');
	Route::delete('/investigaciones/{id}', 'InvestigationsController@destroy');
	// EVENTS
	Route::get('/eventos', 'EventsController@index');
	Route::get('/eventos/{id}', 'EventsController@show');
	Route::post('/eventos', 'EventsController@store');
	Route::put('/eventos/{id}', 'EventsController@update');
	Route::get('/eventos/{id}/activar', 'EventsController@activate');
	Route::get('/eventos/{id}/desactivar', 'EventsController@desactivate');
	Route::delete('/eventos/{id}', 'EventsController@destroy');
	// MAILS
	Route::get('/mensajes', 'EmailsController@index');
	Route::get('/mensajes/{id}', 'EmailsController@show');
	Route::post('/mensajes/enviar', 'EmailsController@sendEmail');
	// USERS
	Route::get('/usuarios', 'ProfilesController@indexUser');
	Route::get('/usuarios/{id}', 'ProfilesController@showUser');
	Route::post('/usuarios', 'ProfilesController@storeUser');
	Route::put('/usuarios/{id}', 'ProfilesController@updateUser');
	Route::get('/usuarios/{id}/activar', 'ProfilesController@activate');
	Route::get('/usuarios/{id}/desactivar', 'ProfilesController@desactivate');
	Route::delete('/usuarios/{id}', 'ProfilesController@destroy');
	// CONFIGURATIONS
	Route::get('/configuracion', 'ConfigurationsController@show');
	Route::put('/configuracion/{id}', 'ConfigurationsController@update');
	// LOGACTIVITIES
	Route::get('/logactividad', 'LogactivitiesController@index');

	Route::any('{catchall}', function ( $page ) {
		return responseKO('404', Lang::get('messages.pagina.noencontrada') . $page);
	})->where('catchall', '(.*)');
});

