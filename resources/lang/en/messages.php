<?php

return [
	'trabajo' => [
		'anadido' => 'Job added',
		'noanadido' => 'Job not added',
		'actualizado' => 'Job updated',
		'noactualizado' => 'Job not updated',
		'eliminado' => 'Job deleted',
		'noeliminado' => 'Job not deleted',
		'activado' => 'Job activated',
		'noactivado' => 'Job not activated',
		'desactivado' => 'Job desactivated',
		'nodesactivado' => 'Job not desactivted',
		'noencontrado' => 'Job not found'
	],
];
