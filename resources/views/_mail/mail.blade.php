<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #303030;
                display: table;
                font-family: 'Lato', sans-serif;
            }
            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
            .content {
                text-align: left;
                display: inline-block;
            }
            .title {
                font-size: 20px;
                margin-bottom: 10px;
				color: #0093C9;
                text-align: left;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">{{ $subject }}</div>
				{{ $messagge }}
            </div>
        </div>
    </body>
</html>
