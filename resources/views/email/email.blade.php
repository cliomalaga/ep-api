<!DOCTYPE html>
<html>
    <head>
        <style>
            html, body {
                height: 100%;
            }
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #303030;
                display: table;
            }
            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
            .content {
                text-align: left;
                display: inline-block;
            }
            .title {
                font-size: 20px;
                margin-bottom: 5px;
				color: #0093C9;
                text-align: left;
            }
            .stitle {
                font-size: 15px;
                margin-bottom: 15px;
                text-align: left;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="subtitle">Enviado desde la Administración de Evaluación Psicológica</div>
			<br>
			<div class="title">Usuario :</div>
			<div> {{ $toEmail }}</div>
			<br>
			<div class="title">Asunto :</div>
			<div> {{ $subject }}</div>
			<br>
			<div class="title">Mensaje :</div>
			<div>{!! $messagge !!}</div>
        </div>
    </body>
</html>
